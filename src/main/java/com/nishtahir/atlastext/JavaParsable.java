package com.nishtahir.atlastext;

import org.antlr.v4.runtime.*;

/**
 * For class loader to load lexer and parser
 */
public class JavaParsable implements IParsable {

    public JavaParsable(){
    }

    public Parser getParser(TokenStream tokenStream) {
        return new JavaParser(tokenStream);
    }


    public Lexer getLexer(CharStream input) {
        return new JavaLexer(input);
    }

}
