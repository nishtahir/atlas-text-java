package com.nishtahir.atlastext;

import org.antlr.v4.runtime.*;

/**
 *
 */
public interface IParsable {

    Parser getParser(TokenStream tokenStream);

    Lexer getLexer(CharStream input);

}
